package newcoffeemaker;

public interface ICoffeeMachineContainer {
    void add(double count);
    double getCurrentCount();
    double getFreeSpace();
    int getMaxCount();
    void remove(double count);
    void removeAll();
}


package newcoffeemaker;

public enum CoffeeMakingResult {
    
    Succes,
    NotEnoughCoffee,
    NotEnoughWater,
    NotEnoughFreeSpace;
    
}

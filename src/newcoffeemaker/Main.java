
package newcoffeemaker;

public class Main {
    
     public static void main(String[] args) {
         
         CoffeeMachine myCoffeeMachine = new CoffeeMachine();

         String status = myCoffeeMachine.getStatus();
         System.out.println(status + "\n");
         
         myCoffeeMachine.addResource(ResourceType.Water, 4);
         myCoffeeMachine.addResource(ResourceType.Coffee, 2);
         
         status = myCoffeeMachine.getStatus();
         System.out.println(status + "\n");
         
         //myCoffeeMachine.makeCoffee(CoffeeType.Espresso, 2);
         
         //status = myCoffeeMachine.getStatus();
         //System.out.println(status + "\n");
         
         //System.out.println(myCoffeeMachine.CoffeeMakingCount(CoffeeType.Espresso));
         
         System.out.println(myCoffeeMachine.CoffeeMakeResult(CoffeeType.Espresso, 100));    
        
    }
}

package newcoffeemaker;

import java.util.ArrayList;
import java.util.HashMap;

public class CoffeeMachine{

    private HashMap<ResourceType, ICoffeeMachineContainer> coffeeMachineContainer = new HashMap<>();

    public CoffeeMachine() {
        coffeeMachineContainer.put(ResourceType.Water, new CoffeeMachineContainer(ResourceType.Water, 10));
        coffeeMachineContainer.put(ResourceType.Coffee, new CoffeeMachineContainer(ResourceType.Coffee, 6));
        coffeeMachineContainer.put(ResourceType.CoffeeGrounds, new CoffeeMachineContainer(ResourceType.CoffeeGrounds, 6));
    }
    
    public String getStatus() {
        StringBuilder builder = new StringBuilder();
        builder.append("The coffeemaker's status is").
                append(System.getProperty("line.separator"));
        
        WriteResourceCount(builder, ResourceType.Coffee);
        builder.append(System.getProperty("line.separator"));
        
        WriteResourceCount(builder, ResourceType.Water);
        builder.append(System.getProperty("line.separator"));
        
        WriteResourceCount(builder, ResourceType.CoffeeGrounds);
        builder.append(System.getProperty("line.separator"));

        return builder.toString();
    }
    
    private void WriteResourceCount(StringBuilder builder, ResourceType type){
        builder.append(type.toString()).
                append(" level: ").
                append(((int)coffeeMachineContainer.get(type).getCurrentCount())).
                append("/").
                append(coffeeMachineContainer.get(type).getMaxCount()).
                append(" (").
                append(coffeeMachineContainer.get(type).getCurrentCount() / coffeeMachineContainer.get(type).getMaxCount()* 100).
                append("%)");
    }
    
    public void addResource(ResourceType type, int count){         
        coffeeMachineContainer.get(type).add(count); 
    }
    
    public void makeCoffee(CoffeeType type, int count){
        if (coffeeMachineContainer.get(ResourceType.Coffee).getCurrentCount() < type.getCoffeeCount()*count)
            throw new RuntimeException("There isn't enough coffee in the machine");
        
        if (coffeeMachineContainer.get(ResourceType.Water).getCurrentCount() < type.getWaterCount()*count)
            throw new RuntimeException("There isn't enough water in the machine");
        
        if (coffeeMachineContainer.get(ResourceType.CoffeeGrounds).getFreeSpace() < type.getCoffeeCount()*count)
            throw new RuntimeException("There isn't enough ground space in the machine");
        
        coffeeMachineContainer.get(ResourceType.Coffee).remove(type.getCoffeeCount()*count);
        coffeeMachineContainer.get(ResourceType.Water).remove(type.getWaterCount()*count);
        coffeeMachineContainer.get(ResourceType.CoffeeGrounds).add(type.getCoffeeCount()*count);         
    }
    
    public int CoffeeMakingCount(CoffeeType type){
        
        int countOfCoffee, countOfWater, countOfGround, smallest;

        countOfCoffee = (int)(coffeeMachineContainer.get(ResourceType.Coffee).getCurrentCount() / type.getCoffeeCount()); 
        countOfWater = (int)(coffeeMachineContainer.get(ResourceType.Water)).getCurrentCount() / type.getWaterCount();
        countOfGround = (int)(coffeeMachineContainer.get(ResourceType.CoffeeGrounds).getFreeSpace()/ type.getCoffeeCount());
        
        smallest = Math.min(countOfCoffee, Math.min(countOfGround, countOfWater));
             
        return smallest;
    }
    
    public ArrayList<CoffeeMakingResult> CoffeeMakeResult (CoffeeType type, int count){

        ArrayList<CoffeeMakingResult> res = new ArrayList<>();
        
        double coffeeRequirement = type.getCoffeeCount() * count;
        double waterRequirement = type.getWaterCount() * count;
        double freeSpaceRequirement = type.getCoffeeCount() * count;
        
        if (coffeeRequirement > coffeeMachineContainer.get(ResourceType.Coffee).getCurrentCount())
            res.add(CoffeeMakingResult.NotEnoughCoffee);
        
        if (waterRequirement > coffeeMachineContainer.get(ResourceType.Water).getCurrentCount())
            res.add(CoffeeMakingResult.NotEnoughWater);
                    
        if (freeSpaceRequirement > type.getCoffeeCount() * count)
            res.add(CoffeeMakingResult.NotEnoughFreeSpace);
        
        if(res.isEmpty())
            res.add(CoffeeMakingResult.Succes);
                
        return res;
       
    }
}

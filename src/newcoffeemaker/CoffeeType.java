
package newcoffeemaker;

public enum CoffeeType {
    Espresso(0.5, 1),
    Capuccino(1, 2),
    Lungo(2, 2);
    
    private final double coffeeCount;
    private final int waterCount;
    
    private CoffeeType(double coffeeCount, int waterCount){
        this.coffeeCount = coffeeCount;
        this.waterCount = waterCount;
    }

    public double getCoffeeCount() {
        return coffeeCount;
    }

    public int getWaterCount() {
        return waterCount;
    }
}
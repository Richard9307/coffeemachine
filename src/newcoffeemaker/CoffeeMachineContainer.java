
package newcoffeemaker;

public class CoffeeMachineContainer implements ICoffeeMachineContainer {
    
    private ResourceType ContainerType;
    private int maxCount;
    private double currentCount = 0;
    
    public CoffeeMachineContainer(ResourceType type, int maxCount){
        ContainerType = type;
        this.maxCount = maxCount;
    }

    @Override
    public double getCurrentCount() {
        return currentCount;
    }
    @Override
    public int getMaxCount() {
        return maxCount;
    }
    @Override
    public double getFreeSpace(){
        return maxCount - currentCount;
    }
    
    @Override
    public void add(double count){
        if(currentCount + count > maxCount)
            throw new RuntimeException("You can't add that much resource");
        
        this.currentCount += count; 
    }
    
    @Override
    public void remove(double count){
        if(currentCount < count)
            throw new RuntimeException("You can't remove that much resource");
        
        this.currentCount -= count; 
    }
    @Override
    public void removeAll(){
        this.currentCount = 0; 
    }
}
